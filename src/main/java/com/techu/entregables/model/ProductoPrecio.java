package com.techu.entregables.model;

public class ProductoPrecio {
    String id;
    double precio;

    public ProductoPrecio() {};

    public ProductoPrecio(String id, double precio) {
        this.id = id;
        this.precio = precio;
    }

    public String getId() {
        return id;
    }

   public double getPrecio() {
        return precio;
    }

    public void setPrecio(double precio) {
        this.precio = precio;
    }
}
