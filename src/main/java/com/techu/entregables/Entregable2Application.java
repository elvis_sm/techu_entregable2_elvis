package com.techu.entregables;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Entregable2Application {

	public static void main(String[] args) {
		SpringApplication.run(Entregable2Application.class, args);
	}

}
