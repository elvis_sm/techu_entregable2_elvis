package com.techu.entregables.service;

import com.techu.entregables.model.Producto;
import com.techu.entregables.repository.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import org.springframework.data.mongodb.core.MongoOperations;


@Service
public class ProductoMongoService {

    @Autowired
    ProductoRepository productoRepository;
    @Autowired
    MongoOperations mo;

    // listar
    public List<Producto> findAll(){
        return productoRepository.findAll();
    }

    // crear
    public Producto save(Producto newProducto){
        return productoRepository.save(newProducto);
    }

    // buscar por id
    public Optional<Producto> findById(String id){
       return productoRepository.findById(id);
    }

    // buscar por id
    public Producto myfindById(String id){
        return mo.findOne(new Query(Criteria.where("_id").is(id)), Producto.class);
    }

    // eliminar
    public boolean delete(Producto productoModel) {
        try {
            productoRepository.delete(productoModel);
            return true;
        } catch(Exception ex) {
            return false;
        }
    }
}
