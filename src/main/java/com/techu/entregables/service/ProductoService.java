package com.techu.entregables.service;

import com.techu.entregables.model.Producto;
import com.techu.entregables.model.Usuario;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductoService {

    private List<Producto> productoList = new ArrayList<>();

    public ProductoService() {
        productoList.add(new Producto("1", "producto 1", 100.50));
        productoList.add(new Producto("2", "producto 2", 150.00));
        productoList.add(new Producto("3", "producto 3", 100.00));
        productoList.add(new Producto("4", "producto 4", 50.50));
        productoList.add(new Producto("5", "producto 5", 103.50));
        List<Usuario> users = new ArrayList<>();
        users.add(new Usuario("1"));
        users.add(new Usuario("3"));
        users.add(new Usuario("5"));
        productoList.get(1).setUsers(users);
    }

    // READ productos
    public List<Producto> getProductos() {
        return productoList;
    }

    // READ instance (por ID)
    public Producto getProductoById(String id) {
        int idx = Integer.parseInt(id);
        if(getIndex(idx)>=0){
            return productoList.get(getIndex(idx));
        }
        return null;
    }

    // CREATE productos
    public Producto addProducto(Producto nuevoProducto) {
        productoList.add(nuevoProducto);
        return nuevoProducto;
    }

    // UPDATE
    public Producto updateProductoById(int index, Producto newPro){
        int pos = getIndex(index);
        if(pos>=0) {
            productoList.set(pos, newPro);
            return productoList.get(pos);
        }
        return null;
    }

    // UPDATE patch precio
    public Producto updateProductoPrecioById(String index, double newPrecio){
        int idx = Integer.parseInt(index);
        int pos = getIndex(idx);

       // productoList.get(pos).setPrecio(newPrecio);

        if(pos>=0) {
            Producto pr = productoList.get(pos);
            pr.setPrecio(newPrecio);
            productoList.set(pos, pr);
            return productoList.get(pos);
        }
        return null;
    }

    // DELETE
    public void removeProductoById(String index) {
       // int pos = productoList.indexOf(productoList.get(index - 1));
        int idx = Integer.parseInt(index);
        int pos = getIndex(idx);
        if(pos>=0) {
            productoList.remove(pos);
        }
    }

    // Devuelve la posición de un producto en productoList
    public int getIndex(int index) {
        int i = 0;
        while (i < productoList.size()) {
            if (Integer.parseInt(productoList.get(i).getId()) == index) {
                return(i);
            }
            i++;
        }
        return -1;
    }
}
