package com.techu.entregables.controller;

import com.techu.entregables.model.Producto;
import com.techu.entregables.service.ProductoMongoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("${url.base}")
public class ProductoController {

    @Autowired
    private ProductoMongoService productoService;

    @GetMapping("")
    public String home(){
        return "API REST Entregable2";
    }

    // Listar todos los productos
    @GetMapping("/productos")
    public List<Producto> getProductos() {
        return productoService.findAll();
    }

    // Crear un nuevo producto
    @PostMapping("/productos")
    public Producto postProducto(@RequestBody Producto newProduct) {
        productoService.save(newProduct);
        return newProduct;
    }

    // Obtener producto por ID
    @GetMapping("/productos/{id}")
    public Producto getProductoById(@PathVariable String id){
       // return productoService.findById(id);
        return productoService.myfindById(id);
    }

    // Actualizar un producto
    @PutMapping("/productos")
    public void putProducto(@RequestBody Producto productoToUpdate){
        productoService.save(productoToUpdate);
    }

    // Eliminar un producto
    @DeleteMapping("/productos")
    public boolean deleteProducto(@RequestBody Producto productoToDelete){
        return productoService.delete(productoToDelete);
    }
}
